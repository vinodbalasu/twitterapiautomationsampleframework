package exceptions;

import restassured.VinResponse;

public class APIAssertionException extends Exception {

    private static final long serialVersionUID = 1L;

    private VinResponse vinResponse;


    /**
     * Constructs a APIAssertionException with no detail message. A detail
     * message is a String that describes this particular exception.
     */
    public APIAssertionException() {
        super();
    }

    /**
     * Constructs a APIAssertionException with the specified detail message.
     * A detail message is a String that describes this particular
     * exception.
     *
     * @param message the detail message.
     */
    public APIAssertionException(String message) {
        super(message);
    }
    /**
     * Constructs a APIAssertionException with the specified detail message and the Vin response.
     * A detail message is a String that describes this particular
     * exception
     * @param msg the detail message.
     * @param vinResponse the failing response to be stored
     */
    public APIAssertionException(String msg, VinResponse vinResponse){
        super(msg);
        this.vinResponse = vinResponse;
    }


    public VinResponse getVinResponse() {
        return vinResponse;
    }



}

