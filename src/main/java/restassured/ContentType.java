package restassured;

public enum ContentType {

    APPLICATION_JSON("application/json"),
    FORM_URL_ENCODED("application/x-www-form-urlencoded"),
    MULTIPART_FORM_DATA("multipart/form-data"),
    VIDEO_MP4("video/mp4"),
    TEXT_HTML("text/html");


    public String getContentType() {
        return contentType;
    }

    private final String contentType;

    ContentType(String contentType){
        this.contentType = contentType;
    }
}