package restassured;

import dto.twitter.TwitterAuthToken1A;
import lombok.Data;

import java.util.Map;

@Data
public class RequestSpecificationBuilder {
    private String contentType;
    private String baseUrl;
    private String content;
    private Map<String,String> cookies;
    private Map<String,String> headers;
    private Map<String,Object> queryParams;
    private String xcsrfToken;
    private Object contentObj;
    private String filePath;

    public boolean isFollowRedirect() {
        return followRedirect;
    }

    public RequestSpecificationBuilder withFollowRedirect(boolean followRedirect) {
        this.followRedirect = followRedirect;
        return this;
    }

    private boolean followRedirect = true;


    //Specific to twitter
    private TwitterAuthToken1A authToken1A;

    public RequestSpecificationBuilder withContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public RequestSpecificationBuilder withBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public RequestSpecificationBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public RequestSpecificationBuilder withCookies(Map<String, String> cookies) {
        this.cookies = cookies;
        return this;
    }

    public RequestSpecificationBuilder withHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public RequestSpecificationBuilder withQueryParams(Map<String, Object> queryParams) {
        this.queryParams = queryParams;
        return this;
    }

    public RequestSpecificationBuilder withXcsrfToken(String xcsrfToken) {
        this.xcsrfToken = xcsrfToken;
        return this;
    }

    public RequestSpecificationBuilder withContentObj(Object contentObj) {
        this.contentObj = contentObj;
        return this;
    }

    public RequestSpecificationBuilder withFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public RequestSpecificationBuilder withAuthToken1A(TwitterAuthToken1A authToken1A) {
        this.authToken1A = authToken1A;
        return this;
    }

    public RequestSpecificationBuilder build() {
        return this;
    }

}
