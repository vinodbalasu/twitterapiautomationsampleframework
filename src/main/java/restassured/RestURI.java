package restassured;

public enum RestURI {
    //Twitter Api's
    TWITTER_V1_1("https://api.twitter.com/1.1"),
    POST_TWEET("/statuses/update.json"),
    DESTROY_TWEET("/statuses/destroy/:id.json"),
    POST_RETWEET("/statuses/retweet/:id.json"),
    POST_UNRETWEET("/statuses/unretweet/:id.json"),
    GET_TWEET("/statuses/show.json");


    public String getURLPath() {
        return URLPath;
    }

    private final String URLPath;


    RestURI(String urlPath) {
        URLPath = urlPath;
    }

}
