package restassured;

public enum HttpMethod{
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    OPTIONS;
}
