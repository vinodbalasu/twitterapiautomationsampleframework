package restassured;

import lombok.Getter;
import lombok.Setter;

import java.io.InputStream;
import java.util.Map;

@Getter @Setter
public class VinResponse {
    private long responseTime;
    private String responseBody;
    private int statusCode;
    private String statusLine;
    private Map<String,String> cookies;
    private InputStream responseAsStream;
    private Map<String,String> headers;
}
