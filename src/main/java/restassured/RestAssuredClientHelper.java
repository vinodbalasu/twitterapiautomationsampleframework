package restassured;

import com.google.gson.Gson;
import dto.twitter.TwitterAuthToken1A;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.RedirectConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RestAssuredClientHelper {
    private boolean debug = false;
    private final String PROTOCOL_VERSION = "TLSv1.2";
    private boolean followRedirect = true;



    VinResponse executeRequest(HttpMethod requestType, String baseUrl, Map<String, String> cookies,
                               Map<String, String> headers, Map<String, Object> queryParams,
                               String contentType, String content, TwitterAuthToken1A authToken1A, Map<String, Object>... additionalParams) {


        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        RequestSpecification requestSpecification;
        Response response;
        if(cookies!=null && !cookies.isEmpty()){
            requestSpecBuilder.addCookies(cookies);
        }
        if(headers!=null && !headers.isEmpty()){
            requestSpecBuilder.addHeaders(headers);
        }
        if(queryParams!=null && !queryParams.isEmpty()){
            requestSpecBuilder.addQueryParams(queryParams);
        }
        if(contentType!=null){
            requestSpecBuilder.setContentType(contentType);
        }
        if(content!=null) {
            requestSpecBuilder.setBody(content);
        }

        if(additionalParams!=null && additionalParams.length != 0){
            Map<String,Object> params = additionalParams[0];
            if(params.get("filePath")!=null){
                String filePath = (String)params.get("filePath");
                File f1 = new File(filePath);
                if(!f1.exists()){
                    Assert.fail("File Not Found " + filePath);
                }
                else
                    requestSpecBuilder.addMultiPart(f1);
            }
        }
        if(debug){
            requestSpecification = requestSpecBuilder.build().log().all();
        }
        else{
            requestSpecification = requestSpecBuilder.build();
        }


        requestSpecification.auth().oauth(authToken1A.getConsumerKey(), authToken1A.getConsumerSecretKey(),
                authToken1A.getAccessToken(),authToken1A.getAccessTokenSecretKey());

        long startTime = System.currentTimeMillis();

        response = execute(baseUrl, requestType,requestSpecification );
        long endTime = System.currentTimeMillis();
        long responseTime = endTime - startTime;
        VinResponse vinResponse = processRestAssuredResponse(response,responseTime);
        if(isApiLoggingEnabled()) {
            writeToApiLogs(baseUrl, content, headers, vinResponse);
        }
        return vinResponse;
    }


    Response execute(String baseUrl, HttpMethod requestType, RequestSpecification specification){
        Response response = null;
        RedirectConfig redirectConfig = new RedirectConfig().followRedirects(followRedirect);
        SSLConfig sslConfig = SSLConfig.sslConfig().relaxedHTTPSValidation(PROTOCOL_VERSION);
        //Global override for TLS v1.2 since not all tests are using ApiClient.
        RestAssured.config = RestAssuredConfig.config().sslConfig(sslConfig);

        RestAssuredConfig config = RestAssured.config()
                .redirect(redirectConfig)
                .sslConfig(sslConfig);

        try {

            switch (requestType) {
                case GET:
                    response = RestAssured.given().config(config).spec(specification).get(baseUrl);
                    break;

                case POST:
                    response = RestAssured.given().config(config).spec(specification).post(baseUrl);
                    break;

                case PUT:
                    response = RestAssured.given().config(config).spec(specification).put(baseUrl);
                    break;

                case PATCH:
                    response = RestAssured.given().config(config).spec(specification).patch(baseUrl);
                    break;

                case DELETE:
                    response = RestAssured.given().config(config).spec(specification).delete(baseUrl);
                    break;
            }
        }catch (Exception e){
           e.printStackTrace();
        }
        return response;
    }


    public VinResponse executeHttpRequest(HttpMethod requestType,
                                          RequestSpecificationBuilder requestSpecificationBuilder) {
        Map<String,Object> additionalParams = new HashMap();

        //Adding support for setting content as Object
        if(requestSpecificationBuilder.getContentObj()!=null){
            if(requestSpecificationBuilder.getContentType().equalsIgnoreCase(ContentType.APPLICATION_JSON.toString())){
                String content = null;
                if(!(requestSpecificationBuilder.getContentObj() instanceof String)) {
                    content = new Gson().toJson(requestSpecificationBuilder.getContentObj());
                }
                else
                    content = (String)requestSpecificationBuilder.getContentObj();

                requestSpecificationBuilder.withContent(content);
            }
        }

        if(!requestSpecificationBuilder.isFollowRedirect()){
            followRedirect = false;
        }
        if(requestSpecificationBuilder.getFilePath()!=null) {
            additionalParams.put("filePath",requestSpecificationBuilder.getFilePath());
        }

        return executeRequest(requestType, requestSpecificationBuilder.getBaseUrl(),
                requestSpecificationBuilder.getCookies(), requestSpecificationBuilder.getHeaders(),
                requestSpecificationBuilder.getQueryParams(), requestSpecificationBuilder.getContentType(),
                requestSpecificationBuilder.getContent(), requestSpecificationBuilder.getAuthToken1A(),additionalParams);
    }


    private VinResponse processRestAssuredResponse(Response response,long responseTime){

        VinResponse vinResponse = new VinResponse();
        if (response != null) {
            vinResponse.setResponseTime(responseTime);

            if (response.getBody() != null)
                vinResponse.setResponseBody(response.getBody().asString());

            if (response.asInputStream() != null)
                vinResponse.setResponseAsStream(response.asInputStream());

            vinResponse.setStatusCode(response.getStatusCode());
            vinResponse.setStatusLine(response.getStatusLine());
            vinResponse.setCookies(response.getCookies());

            if (response.getHeaders() != null && response.getHeaders().size() > 0) {
                Map<String, String> responseHeaders = new HashMap<String, String>();
                for (Header header : response.getHeaders())
                    responseHeaders.put(header.getName(), header.getValue());
                vinResponse.setHeaders(responseHeaders);
            }
        }
        return vinResponse;
    }


    private void writeToApiLogs(String url,String content,Map<String,String> headers,VinResponse vinResponse){
        String threadName = Thread.currentThread().getName();
        int statusCode = vinResponse.getStatusCode();
        long responseTime = vinResponse.getResponseTime();
        String responseBody = vinResponse.getResponseBody();
        String jobName = System.getProperty("jobName");
        String buildNumber = System.getProperty("buildNumber");

        headers = (headers==null)? Collections.EMPTY_MAP:headers;
        content = (content==null)? "":content;
        responseBody = (statusCode == 200)?"":responseBody;

       /* log.debug(jobName+" "+ buildNumber +" " + threadName +" " +  url +" "+ headers +" " + content +" "  +
                statusCode +"  " +  responseBody + " " +  responseTime);*/
    }

    private boolean isApiLoggingEnabled(){
        return Boolean.valueOf(System.getProperty("enableApiLogging"));
    }


}
