package apitests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import common.AssertApiResponse;
import common.PropertiesHelper;
import dataproviders.twitter.QuotesReader;
import dto.twitter.Tweet;
import dto.twitter.TwitterAuthToken1A;
import exceptions.APIAssertionException;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.annotations.*;
import restassured.*;

import java.lang.reflect.Type;

@Slf4j
public class TwitterApisValidation extends BaseApiTest {

    public static final String CONSUMER_KEY = "consumerKey";
    public static final String CONSUMER_SECRET = "consumerSecretKey";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String ACCESS_SECRET = "accessSecret";
    private final String resourceFile = "/Users/balvinod/Downloads/ApiAutomation/src/main/resources/api/twitter/twitter.properties";

    private TwitterAuthToken1A twitterAuthToken1A;


    @BeforeTest
    public void beforeTest() throws Exception {
        //Populate Authentication details
        final PropertiesHelper propertiesHelper = PropertiesHelper.getInstance();
        propertiesHelper.setResourceFile(resourceFile);
        twitterAuthToken1A = new TwitterAuthToken1A.TwitterAuthToken1ABuilder()
                .accessToken(propertiesHelper.getStringForId(ACCESS_TOKEN))
                .accessTokenSecretKey(propertiesHelper.getStringForId(ACCESS_SECRET))
                .consumerKey(propertiesHelper.getStringForId(CONSUMER_KEY))
                .consumerSecretKey(propertiesHelper.getStringForId(CONSUMER_SECRET)).build();


    }

    @AfterTest
    public void afterTest() {
    }

    @BeforeClass
    public void beforeClass() {

    }

    @AfterClass
    public void afterClass() {

    }

    @Test(dataProvider = "getQuotesData", dataProviderClass = QuotesReader.class)
    public void postTweet(String quoteToPost) throws APIAssertionException {
        final String url = RestURI.TWITTER_V1_1.getURLPath() + RestURI.POST_TWEET.getURLPath() + "?status=" + quoteToPost;
        System.out.println("Url is " + url);
        final RequestSpecificationBuilder requestSpecificationBuilder = new RequestSpecificationBuilder()
                .withContentType(ContentType.APPLICATION_JSON.getContentType())
                .withAuthToken1A(twitterAuthToken1A).withBaseUrl(url).build();

        RestAssuredClientHelper restAssuredClientHelper = new RestAssuredClientHelper();
        final VinResponse response =
                restAssuredClientHelper.executeHttpRequest(HttpMethod.POST, requestSpecificationBuilder);

        //assert api response statusline and status message.
        //Type tokenize the response to appropriate DTO and validate the schema and check for any non null fields.
        AssertApiResponse.assertStatusCode(200, response.getStatusCode());
        System.out.println("Response in text is : " + response.getResponseBody());
        final Type tweetType = new TypeToken<Tweet>(){}.getType();
        final Tweet tweet = new Gson().fromJson(response.getResponseBody(), tweetType);
        final ObjectMapper objectMapper = new ObjectMapper();

        try {
            log.debug("Tweet DTO is : {}", objectMapper.writeValueAsString(tweet));
            System.out.println("Tweet DTO is : " +  objectMapper.writeValueAsString(tweet));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //assert for Id
        Assert.assertNotNull(tweet.getId(), "Tweet Id is missing");
    }

    @Test
    public void destroyTweet() {
    }

    @Test
    public void getTweets() {

    }

    @Test(dataProvider = "getQuotesData", dataProviderClass = QuotesReader.class)
    public void postRetweet() {

    }

    @Test
    public void postUnRetweet() {

    }

    @Test
    public void getRettweet() {

    }
}
