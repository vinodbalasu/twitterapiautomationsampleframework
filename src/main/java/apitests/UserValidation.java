package apitests;

import common.HttpClientHelper;
import common.JSONUtils;
import dataproviders.UserDataReader;
import org.apache.log4j.Logger;
import org.testng.annotations.*;

import java.io.IOException;


public class UserValidation extends BaseApiTest {
    public static Logger log = Logger.getLogger(UserValidation.class);

    private String lhsURL;
    private String rhsURL;

    @Factory(dataProvider = "getUserData", dataProviderClass = UserDataReader.class)
    public UserValidation(String url1, String url2) {
        lhsURL = url1;
        rhsURL = url2;
    }

    @BeforeTest
    public void beforeTest() {
    }

    @AfterTest
    public void afterTest() {
    }

    @BeforeClass
    public void beforeClass() {

    }

    @AfterClass
    public void afterClass() {

    }

    @Test
    public void validateResponses() {
        //make the rest call and fetch the responses
        String response1 = null;
        String response2 = null;

        try {
            response1 = HttpClientHelper.get(lhsURL);
            response2 = HttpClientHelper.get(rhsURL);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //pass the json values to compare the results.
        boolean isMatching = JSONUtils.areEqual(response1, response2);
        if (isMatching) {
            System.out.println(lhsURL + " equals " + rhsURL);
        } else {
            System.out.println(lhsURL + " not equals " + rhsURL);
        }

    }
}

