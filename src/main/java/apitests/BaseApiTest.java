package apitests;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public abstract class BaseApiTest {

    @BeforeSuite
    public void beforeSuite() {
        System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
        PropertyConfigurator.configure("/Users/balvinod/Downloads/ApiAutomation/src/test/resources/log4j.properties");
    }

    @AfterSuite
    public void afterSuite() {
    }

}
