package dataproviders.twitter;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

@Slf4j
//read the quotes from the excel and return any one randomly
public class QuotesReader {
    public static final String SHEET_NAME = "Famous Sports Quotes";


    @DataProvider(name = "getQuotesData")
    public static Object[][] testData() {
        File inputFile = new File("/Users/balvinod/Downloads/ApiAutomation/src/main/resources/api/twitter/QuotesToPostInTwitter.xlsx");

        String[][] rows = null;
        try {
            FileInputStream excelFile = new FileInputStream(inputFile);
            log.debug("Reading the twitter xlsx file to get tweets to post");
            System.out.println("Inside the testData");

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);

            //Get first/desired sheet from the workbook
            final XSSFSheet sheet = workbook.getSheet(SHEET_NAME);

            System.out.println("Row number : " + sheet.getLastRowNum());
            rows = new String[sheet.getLastRowNum() + 1][1];

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                final Cell cell = row.getCell(0);
                rows[row.getRowNum()][0] = cell.getStringCellValue();
                System.out.println("Tweet is : " + rows[row.getRowNum()][0]);
            }
            excelFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rows;
    }


}
