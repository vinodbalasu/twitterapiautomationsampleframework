package dataproviders;

import org.testng.annotations.DataProvider;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserDataReader {

    @DataProvider(name = "getUserData")
    public static Object[][] testData() {

        final File file1 = new File("/Users/balvinod/Downloads/ApiAutomation/src/main/resources/api/standard/file1.txt");
        final File file2 = new File("/Users/balvinod/Downloads/ApiAutomation/src/main/resources/api/standard/file2.txt");

        final List<String[]> urlsdata = new ArrayList<String[]>();

        try {
            BufferedReader br1 = new BufferedReader(new FileReader(file1));
            BufferedReader br2 = new BufferedReader(new FileReader(file2));
            String url1;
            String url2;

            while ((url1 = br1.readLine()) != null && (url2 = br2.readLine()) != null) {
                String[] urls = {url1, url2};
                urlsdata.add(urls);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return urlsdata.toArray(new Object[0][]);
    }
}

