package retry;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


/*
* The following github reference can be used for retrying - annotation based(Every test can have different number of retry count)
*
* link : https://github.com/virenv/TestngRetryCount
* */
public class VinAnnotationTransformer implements IAnnotationTransformer {

    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        annotation.setRetryAnalyzer(VinRetryAnalyzer.class);
    }

}
