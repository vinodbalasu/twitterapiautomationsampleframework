package retry;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import java.util.concurrent.atomic.AtomicInteger;


/*
 * @see org.testng.IRetryAnalyzer#retry(org.testng.ITestResult)
 *
 * This method decides how many times a test needs to be rerun.
 * TestNg will call this method every time a test fails. So we
 * can put some code in here to decide when to rerun the test.
 *
 * Note: This method will return true if a tests needs to be retried
 * and false if not.
 *
 */
public class VinRetryAnalyzer implements IRetryAnalyzer {

    private static int MAX_RETRY_COUNT = 3;

    AtomicInteger count = new AtomicInteger(MAX_RETRY_COUNT);

    public boolean isRetryAvailable() {
        return (count.intValue() > 0);
    }


    public boolean retry(ITestResult result) {
        boolean retry = false;
        if (!result.isSuccess()) {
            if (isRetryAvailable()) {
                System.out.println("Going to retry test case: " + result.getMethod() + ", " + (MAX_RETRY_COUNT - count.intValue() + 1) + " out of " + MAX_RETRY_COUNT);
                retry = true;
                count.decrementAndGet();
                result.setStatus(ITestResult.SKIP);
            } else {
                result.setStatus(ITestResult.FAILURE);  //If maxCount reached,test marked as failed
            }
        } else {
            result.setStatus(ITestResult.SUCCESS);      //If test passes, TestNG marks it as passed
        }
        return retry;
    }
}
