package common;

import exceptions.APIAssertionException;
import lombok.extern.slf4j.Slf4j;
import restassured.VinResponse;

/*Common class which is used to assert API response status and message*/
@Slf4j
public class AssertApiResponse {

    public static void assertStatusCode(final int expectedStatusCode,  final int actualStatusCode) throws APIAssertionException {

        if (actualStatusCode != expectedStatusCode) {
            String errorMessage = String.format("Actual status code : %s is not matching with expected status code : %s",
                    actualStatusCode, expectedStatusCode);
            log.debug(errorMessage);
            throw new APIAssertionException(errorMessage);
        }
    }


    /*Assertion :
        Response code - we often check for 200/201, 400 Bad Request, 404 Not Found, 302 Redirect etc.
        Response body - Json Schema e.g. are all fields of the correct type?
        Response body - data validation e.g. I’m requesting record 1, does it contain the correct data in all of the fields?
        Response headers - are we exposing too much information? E.g. server name
    */

    public static void assertApiResponseWithFailureMessage(final int expectedStatusCode, final VinResponse response,
                                                           final String failureMessage) throws APIAssertionException {
        final int actualStatusCode = response.getStatusCode();

        if (actualStatusCode != expectedStatusCode) {
            String errorMessage = String.format("Actual status code : {} is not matching with expected status code : {}",
                    actualStatusCode, expectedStatusCode);
            log.debug(errorMessage);
            throw new APIAssertionException(errorMessage + " Failure reason : " + failureMessage +
                    " Actual response body obtained : " + response.getResponseBody());
        }


    }

    public static void assertJSONResponses(final String actualJson, final String expectedJson) throws APIAssertionException {
        boolean areEqual = JSONUtils.areEqual(actualJson, expectedJson);
        if (!areEqual) {
            log.debug("Actual Response is : {}", actualJson);
            log.debug("Expected Response is {}", expectedJson);
            throw new APIAssertionException("Actual Response is not matching with Expected Response");
        }
    }


}
