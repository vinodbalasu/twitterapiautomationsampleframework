package common;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Helper Class for loading the properties and fetching the string values using the Id(key).
 *
 * @author balvinod
 */
@Slf4j
public class PropertiesHelper {
    private static PropertiesHelper propertiesHelper;
    private final Properties properties = new Properties();

    public String getResourceFile() {
        return resourceFile;
    }

    public void setResourceFile(String resourceFile) {
        this.resourceFile = resourceFile;
    }

    private String resourceFile;



    /**
     * Restricting the instantiation of class to one object.
     */
    public static PropertiesHelper getInstance() {
        if (propertiesHelper == null) {
            propertiesHelper = new PropertiesHelper();
        }
        return propertiesHelper;
    }

    /**
     *Load the Properties under resources folder
     */
    private void loadLocalizableProperties() {
        try {
            final InputStream input = new FileInputStream(resourceFile);
            properties.load(new InputStreamReader(input, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     *Get the String Value for the particular id which is stored as key in the properties file
     *
     * @param id
     *@return String
     *
     */
    public String getStringForId(final String id) throws Exception {
        String label = null;

        loadLocalizableProperties();
        if (!properties.isEmpty()) {
            log.debug("String Id is : {}", id);
            label = properties.getProperty(id);
            if (label == null) {
                final String errorMessage = String.format("No Strings found for the Id : %s. Test Execution "
                        + "stopped", id);
                throw new Exception(errorMessage);
            }
            log.debug("String value returned is : {}", label);
        }
        return label;
    }

}

