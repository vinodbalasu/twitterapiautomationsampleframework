package dto.twitter;

import lombok.Data;

@Data
public class User {

    private String friends_count;

    private String profile_image_url_https;

    private String listed_count;


    private String default_profile_image;

    private String favourites_count;

    private String description;

    private String created_at;

    private String screen_name;

    private String id_str;


    private String id;

    private String verified;

    private String url;

    private Entities entities;

    private String statuses_count;

    private String followers_count;

    private String default_profile;


    private String name;

    private String location;



    @Data
    public class Entities {
        private Description description;

        private Url url;
    }

}



