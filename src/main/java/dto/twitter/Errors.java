package dto.twitter;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Errors {
    private String code;

    private String message;

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [code = "+code+", message = "+message+"]";
    }
}
