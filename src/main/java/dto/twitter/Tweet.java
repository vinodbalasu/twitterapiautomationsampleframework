package dto.twitter;

import lombok.Data;

@Data
public class Tweet {
    private String in_reply_to_status_id_str;
    private int in_reply_to_status_id;
    private String created_at;
    private String truncated;
    private String in_reply_to_user_id_str;
    private String source;
    private String retweet_count;
    private String retweeted;
    private String in_reply_to_screen_name;

    private String is_quote_status;
    private Entities entities;
    private String id_str;
    private int in_reply_to_user_id;
    private String favorite_count;
    private String id;
    private String text;
    private String lang;
    private User user;
    private String favorited;

}