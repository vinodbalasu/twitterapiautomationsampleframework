package dto.twitter;

import lombok.Data;

@Data
public class TwitterAuthToken1A {
    private String consumerKey;
    private String consumerSecretKey;
    private String accessToken;
    private String accessTokenSecretKey;


    public TwitterAuthToken1A(TwitterAuthToken1ABuilder builder) {
        this.consumerKey = builder.consumerKey;
        this.consumerSecretKey = builder.consumerSecretKey;
        this.accessToken = builder.accessToken;
        this.accessTokenSecretKey = builder.accessTokenSecretKey;
    }


    public static class TwitterAuthToken1ABuilder {
        String consumerKey;
        String consumerSecretKey;
        String accessToken;
        String accessTokenSecretKey;

        public TwitterAuthToken1ABuilder consumerKey(String key) {
            consumerKey = key;
            return this;
        }

        public TwitterAuthToken1ABuilder consumerSecretKey(String key) {
            consumerSecretKey = key;
            return this;
        }

        public TwitterAuthToken1ABuilder accessToken(String token) {
            accessToken = token;
            return this;
        }

        public TwitterAuthToken1ABuilder accessTokenSecretKey(String accessToken) {
            accessTokenSecretKey = accessToken;
            return this;
        }

        public TwitterAuthToken1A build() {
            TwitterAuthToken1A twitterAuthToken1A = new TwitterAuthToken1A(this);
            return twitterAuthToken1A;
        }

    }
}

