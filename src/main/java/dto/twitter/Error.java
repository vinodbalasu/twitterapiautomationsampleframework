package dto.twitter;

public class Error {
    private Errors[] errors;

    public Errors[] getErrors ()
    {
        return errors;
    }

    public void setErrors (Errors[] errors)
    {
        this.errors = errors;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [errors = "+errors+"]";
    }
}