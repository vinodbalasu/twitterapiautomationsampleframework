package dto.twitter;

import lombok.Data;

@Data
public class Url
{
    private Urls[] urls;

    @Data
    public class Urls {
        private String display_url;

        private String[] indices;

        private String expanded_url;

        private String url;
    }
}

