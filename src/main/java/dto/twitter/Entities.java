package dto.twitter;

import lombok.Data;

@Data
public class Entities {
    private String[] urls;

    private String[] hashtags;

    private String[] user_mentions;

    private String[] symbols;
}